# Installatie

## 1. Install Python

Download en install Python https://www.python.org/downloads/

Vergeet niet het vinkje aan te zetten dat 't in PATH moet!

## 2. Install Poetry

Powershell:

`(Invoke-WebRequest -Uri https://install.python-poetry.org/ -UseBasicParsing).Content | python`

Na het uitvoeren hiervan, is het nodig poetry toe te voegen aan het Windows PATH.  

1. Druk op de Windows key en type powershell -> click op Run as Administrator
2. Vul de Kadaster security dialog in
3. Er is nu een Powershell window open. Type het volgende in: SystemPropertiesAdvanced
4. Vul de Kadaster security dialog nogmaal in
5. Er is nu een Environment Variables dialoog open. In de bovenste sectie (User variables for <jouw username>) click op regel Path en dan de Edit… knop.
6. De Edit environment variable dialoog is nu open. Click op de New knop.
7. Voeg de volgende locatie toe: C:\Users\<jouw username>\AppData\Roaming\Python\Scripts
8. Click op de OK button.

Om verder te kunnen, is het nu nodig de PC / laptop te rebooten.

## 3. Install Node JS LTS

Install NodeJS-LTS:

https://nodejs.org/dist/v16.15.1/node-v16.15.1-x64.msi

## 4. Install dependencies

In de gecloonde projectfolder: 

`poetry install`

Dit commando zal de rest van het werk doen.

## 5. Activeer Robot Framework Browser
`poetry run rfbrowser init` 
NodeJS packages en PlayWright browsers worden binnen gehaald