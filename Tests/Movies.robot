*** Settings ***
Resource          ../Keywords/General.resource
Resource          ../Keywords/MoviePage.resource

*** Test Cases ***
First Movie
    Given the user is loggedin as admin
    When the user adds a new movie
    Then the movie is visible in the list
